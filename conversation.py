import random
import time
import user_input
from responders import responder_to_greetings, \
                       responder_to_goodbyes, \
                       responder_to_feelings, \
                       responder_to_mood_questions, \
                       responder_to_remember_questions, \
                       responder_to_any_message

# While talking to the user in a conversation, the user will be asked to input a message.
#
# A list of possible responses to this message is then determined
# by trying different responders to different types of messages like
# greetings, goodbyes, feelings, mood questions, remember questions and any message.
#
# During the conversation a response will randomly be chosen from the possible responses
# and responded to the user.
#
# To prevent repetition of responses the responses given during a conversation are stored.
#
# The user may instruct the chat bot to start over by typing "start over" when prompted for input.


class Conversation:

    START_OVER_MESSAGE = "start over"
    START_OVER_RESPONSE = "OK, starting over."

    def __init__(self):
        # Note: the last responder must always respond to any message to make sure the conversation does not end
        self._responders = [responder_to_greetings.ResponderToGreetings(),
                            responder_to_goodbyes.ResponderToGoodbyes(),
                            responder_to_feelings.ResponderToFeelings(),
                            responder_to_mood_questions.ResponderToMoodQuestions(),
                            responder_to_remember_questions.ResponderToRememberQuestions(),
                            responder_to_any_message.ResponderToAnyMessage()]

        self._responses_given = []

    def talk_with_user(self):
        message = user_input.get_message()
        if message == self.START_OVER_MESSAGE:
            self._respond(self.START_OVER_RESPONSE)
            self._start_over()
        else:
            responses = self._determine_possible_responses_to_message(message)
            response = self._choose_a_response_not_yet_given(responses)
            self._respond(response)
            self._store_response_given(response)
        # for debug purposes we print the responses we have given during the conversation
        self._print_responses_given()

    def _start_over(self):
        self._responses_given = []

    def _determine_possible_responses_to_message(self, message):
        for responder in self._responders:
            responses = responder.determine_possible_responses_to_message(message)
            if responses is not None:
                break  # since the current responder has given some possible responses, we can stop the loop
        return responses

    def _respond(self, response):
        wait_time = random.randint(1, 3)
        time.sleep(wait_time)
        print("BOT: {}".format(response))

    def _choose_a_response_not_yet_given(self, responses):  # preferably give a response not yet given
        responses_to_choose_from = self._determine_responses_to_choose_from(responses)
        response = random.choice(responses_to_choose_from)
        return response

    def _determine_responses_to_choose_from(self, responses):
        # returns responses not yet given when not empty, or else all the responses
        responses_not_yet_given = self._determine_responses_not_yet_given(responses)
        if len(responses_not_yet_given) == 0:
            # all the responses have been given before, choose from the complete set of responses again
            responses_to_choose_from = responses
        else:
            responses_to_choose_from = responses_not_yet_given
        return responses_to_choose_from

    def _determine_responses_not_yet_given(self, responses):
        responses_not_yet_given = list(set(responses) - set(self._responses_given))
        return responses_not_yet_given

    def _store_response_given(self, response):
        self._responses_given.append(response)

    def _print_responses_given(self):
        print("Responses given so far ", self._responses_given)
