from responders import responder_using_pattern_matching


class ResponderToFeelings(responder_using_pattern_matching.ResponderUsingPatternMatching):

    def __init__(self):
        feeling_patterns = ["I feel (.*)"]
        possible_responses_to_feeling = ["Why do you feel {}?",
                                         "You feel {}, why?",
                                         "How long have you been feeling {}?"]

        responder_using_pattern_matching.ResponderUsingPatternMatching.__init__(self,
                                                                                feeling_patterns,
                                                                                possible_responses_to_feeling)
