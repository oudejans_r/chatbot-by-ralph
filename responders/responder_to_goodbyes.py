from responders import responder_using_dictionary


class ResponderToGoodbyes(responder_using_dictionary.ResponderUsingDictionary):

    def __init__(self):
        goodbyes_to_respond_to = ["goodbye", "bye", "bye bye", "so long", "take care", "salut", "see ya", "see you",
                                  "see ya later", "see you later", "ttyl", "talk to you later", "we'll talk later",
                                  "i gotta go", "i gotta go now", "i have to go", "have to go now"]
        possible_responses_to_goodbyes = ["Bye!", "Goodbye!", "See you later", "Talk to you later",
                                          "I was nice to talk to you, bye!", "I'm looking forward to our next chat. :)"]

        responder_using_dictionary.ResponderUsingDictionary.__init__(self,
                                                                     goodbyes_to_respond_to,
                                                                     possible_responses_to_goodbyes)
