class ResponderToAnyMessage:

    possible_responses = ["Ok", "Hmmm", "That's interesting"]

    def determine_possible_responses_to_message(self, message):
        return self.possible_responses
