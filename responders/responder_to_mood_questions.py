from responders import responder_using_dictionary


class ResponderToMoodQuestions(responder_using_dictionary.ResponderUsingDictionary):

    def __init__(self):
        mood_questions_to_respond_to = ["how are you"]
        possible_responses_to_mood_questions = ["I'm good, thanks for asking!", "I'm fine, thanks!"]

        responder_using_dictionary.ResponderUsingDictionary.__init__(self,
                                                                     mood_questions_to_respond_to,
                                                                     possible_responses_to_mood_questions)
