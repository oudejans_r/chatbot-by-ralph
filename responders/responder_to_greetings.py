from responders import responder_using_dictionary


class ResponderToGreetings(responder_using_dictionary.ResponderUsingDictionary):

    def __init__(self):
        greetings_to_respond_to = ["hi", "hello", "goodday", "what's up", "sup", "yo", "hi there", "hey there", "hey",
                                   "greetings earthling", "greetings"]
        possible_responses_to_greeting = ["Hello, how are you?", "Hey,you again!", "Hi, how have you been?",
                                          "Goodday, what's up?"]

        responder_using_dictionary.ResponderUsingDictionary.__init__(self,
                                                                     greetings_to_respond_to,
                                                                     possible_responses_to_greeting)
