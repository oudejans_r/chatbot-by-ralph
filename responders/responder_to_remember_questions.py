from responders import responder_using_pattern_matching


class ResponderToRememberQuestions(responder_using_pattern_matching.ResponderUsingPatternMatching):

    def __init__(self):
        remember_questions_to_respond_to = ["Do you remember (.*)"]
        possible_responses_to_remember_questions = ["Of course I remember {}",
                                                    "Yes, I do remember {}",
                                                    "Now that you mention it, I do remember {}"]

        responder_using_pattern_matching.ResponderUsingPatternMatching.__init__(self,
                                                                                remember_questions_to_respond_to,
                                                                                possible_responses_to_remember_questions)
