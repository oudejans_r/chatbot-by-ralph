class ResponderUsingDictionary:

    def __init__(self, messages_to_respond_to, possible_responses):
        self._messages_to_respond_to = messages_to_respond_to
        self._possible_responses = possible_responses

    def determine_possible_responses_to_message(self, message):
        if message in self._messages_to_respond_to:
            return self._possible_responses
        else:
            return None
