import re


class ResponderUsingPatternMatching:

    def __init__(self, patterns_to_match, possible_responses):
        self._patterns_to_match = patterns_to_match
        self._possible_responses = possible_responses

    def determine_possible_responses_to_message(self, message):
        possible_responses = self._determine_possible_responses_to_message_by_matching_patterns(message)
        return possible_responses

    def _determine_possible_responses_to_message_by_matching_patterns(self, message):
        possible_responses = None
        for pattern in self._patterns_to_match:
            possible_responses = self._determine_possible_responses_to_message_by_matching_pattern(pattern, message)
            if possible_responses is not None:
                break  # since we have some possible responses, we can stop looping over the patterns to match
        return possible_responses

    def _determine_possible_responses_to_message_by_matching_pattern(self, pattern, message):
        match = re.search(pattern, message, re.IGNORECASE)
        if match:
            value_in_message = match.group(1)
            possible_responses = self._format_possible_responses(value_in_message)
        else:
            possible_responses = None
        return possible_responses

    def _format_possible_responses(self, value_in_message):
        possible_responses = [possible_response.format(value_in_message)
                              for possible_response in self._possible_responses]
        return possible_responses
