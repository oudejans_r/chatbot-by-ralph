import unittest
from responders import responder_using_dictionary


class ResponderUsingDictionaryResponseTests(unittest.TestCase):

    def test_given_a_message_not_in_dictionary_then_responder_does_not_respond(self):
        messages_to_respond_to = ["Hi"]
        possible_responses = ["Hello"]
        responder = responder_using_dictionary.ResponderUsingDictionary(messages_to_respond_to, possible_responses)
        message_for_testing = "-- this is not in the dictionary --"
        expected_responses = None
        self._assert_responder_responds_with_the_expected_responses(responder, message_for_testing, expected_responses)

    def test_given_a_message_in_dictionary_then_responder_responds_with_the_expected_responses(self):
        messages_to_respond_to = ["Hi"]
        possible_responses = ["Hello"]
        responder = responder_using_dictionary.ResponderUsingDictionary(messages_to_respond_to, possible_responses)
        message_for_testing = messages_to_respond_to[0]
        expected_responses = ["Hello"]
        self._assert_responder_responds_with_the_expected_responses(responder, message_for_testing, expected_responses)

    def _assert_responder_responds_with_the_expected_responses(self, responder, message_for_testing, expected_responses):
        actual_responses = responder.determine_possible_responses_to_message(message_for_testing)
        self.assertEqual(expected_responses, actual_responses)


if __name__ == '__main__':
    unittest.main()
