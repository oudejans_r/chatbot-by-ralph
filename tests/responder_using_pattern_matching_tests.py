import unittest
from responders import responder_using_pattern_matching


class ResponderUsingPatternMatchingTests(unittest.TestCase):

    def test_given_a_message_not_matching_patterns_then_responder_does_not_respond(self):
        patterns_to_match = ["My name is (.*)"]
        responses = ["Hello {}"]
        responder = responder_using_pattern_matching.ResponderUsingPatternMatching(patterns_to_match, responses)
        message_for_testing = "--this does not match any pattern--"
        expected_responses = None
        self._assert_responder_responds_with_the_excepted_responses(responder, message_for_testing, expected_responses)

    def test_given_a_message_matching_a_pattern_then_responder_responds_with_the_expected_responses(self):
        patterns_to_match = ["My name is (.*)"]
        responses = ["Hello {}"]
        responder = responder_using_pattern_matching.ResponderUsingPatternMatching(patterns_to_match, responses)
        message_for_testing = "My name is Bob"
        expected_responses = ["Hello Bob"]
        self._assert_responder_responds_with_the_excepted_responses(responder, message_for_testing, expected_responses)

    def _assert_responder_responds_with_the_excepted_responses(self, responder, message_for_testing, expected_responses):
        actual_response = responder.determine_possible_responses_to_message(message_for_testing)
        self.assertEqual(expected_responses, actual_response)


if __name__ == '__main__':
    unittest.main()
