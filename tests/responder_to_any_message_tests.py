import unittest
from responders import responder_to_any_message


class ResponderToAnyMessageTests(unittest.TestCase):

    def test_responder_always_responds(self):
        responder = responder_to_any_message.ResponderToAnyMessage()
        possible_responses = responder.determine_possible_responses_to_message("any message")
        self.assertIsNotNone(possible_responses)


if __name__ == '__main__':
    unittest.main()
