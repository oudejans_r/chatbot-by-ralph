PROMPT = ">>>>"


def get_message():
    message = input(PROMPT)
    message = message.lower()
    message = strip_punctuation(message)
    return message


def strip_punctuation(message):
    if message.endswith(('!', '?')):
        message = message[0:-1]
        return message
    else:
        return message

