import conversation


class ChatBot:

    # this chat bot chats by continually talking with the user in a conversation.

    def __init__(self):
        self._conversation = conversation.Conversation()

    def chat(self):
        while True:
            self._conversation.talk_with_user()


if __name__ == '__main__':
    chat_bot = ChatBot()
    chat_bot.chat()
